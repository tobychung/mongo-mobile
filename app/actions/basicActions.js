require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch'
import axios from 'axios'


import {
    createActions
} from 'redux-actions';

import store from '../store';

export const {

    setAllData,
    showPart,
    isMain,
    toggleIntroList,
    toggleVideoList

} = createActions({

    SET_ALL_DATA: (obj) => obj,
    SHOW_PART: (obj) => obj,
    IS_MAIN: (obj) => obj,
    TOGGLE_INTRO_LIST: (obj) => obj,
    TOGGLE_VIDEO_LIST: (obj) => obj,
});