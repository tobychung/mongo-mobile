//-------  CSS  --------//
import './dropdown.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Const ------------//
import {
    styleOpts
} from '../../constants/models';

//---------- Actions ------------//
import {
    toggleIntroList,
    toggleVideoList
} from '../../actions/basicActions'
//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//
import YouTube from 'react-youtube';
//---------- Components ------------//

//---------- Img ------------//
import arrowUpUrl from '../../images/global/arrow-up.png';
import arrowDownUrl from '../../images/global/arrow-down.png';
//==================================================//

class DropdownSubItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {



    }

    render() {

        const opts = {
            height: '220',
            width: '360',
            // width: '100%',
            // height: '100%'
            // playerVars: {
            //     autoplay: 1
            // }
        };

        return (
            <div className="dropdown-sub-li">
               
                <div className='sub-title'>{this.props.title}</div>
                
                <div className='sub-video'>

                    <YouTube
                        videoId={this.props.videoId}
                        className="youtube-container"
                        opts={opts}
                    />

                </div>


                

                

            </div>
        );
    }
};


class DropdownItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this, props);
    }


    handleClick(props, target) {

        let lord = props.lord;

        if (lord == "intro") {

            store.dispatch(toggleIntroList(props));

        } else if (lord == "video") {

            // console.log('[dropdown] props', props);
            store.dispatch(toggleVideoList(props));

        }


    }


    generateSubItem(item, index) {

        return <DropdownSubItem key={index} index={index} title={item.title} videoId={item.videoId} info={item.info}/>
    }

    render() {

        const {
            subArr
        } = this.props;



        let subItems = subArr.map(this.generateSubItem);

        return (
            <li className="dropdown-li"> 
               
                <div className='select-bar' onClick={this.handleClick}>
                    {this.props.title}
                    <img src={this.props.toggle?arrowUpUrl:arrowDownUrl}/>

                </div>
               
                <div className='select-intro' style={this.props.toggle?styleOpts.block:styleOpts.none}>
                    <div>{this.props.info}</div>
                    <img src={this.props.imgUrl}/>
                    <div>{this.props.info2}</div>
                    <img src={this.props.imgUrl2}/>
                    <div>{this.props.info3}</div>
                    <img src={this.props.imgUrl3}/>

                </div>
                <div className='select-video' style={this.props.toggle?styleOpts.block:styleOpts.none}>
                    {subItems}
                </div>

              
               
            </li>
        );
    }
};


class Dropdown extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {
        // console.log('props', );
        return <DropdownItem key={index} lord={item.lord} part={item.part} index={index} title={item.title} subArr={item.subArr} info={item.info} imgUrl={item.imgUrl} info2={item.info2} imgUrl2={item.imgUrl2} info3={item.info3} imgUrl3={item.imgUrl3} toggle={item.toggle}/>
    }

    render() {

        const {
            arr,
            part,
            lord
        } = this.props;



        for (let item of arr) {
            item.lord = lord;
            item.part = part;
        }


        let items = arr.map(this.generateItem);
        return (
            <ul className="dropdown-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(Dropdown);