//-------  CSS  --------//
import './header.min.css';

//-------  Basic  --------//
import React, {

    Component,
    PropTypes
} from 'react';
import ReactDOM from 'react-dom';
//-------  Router  --------//


import {
    Router,
    Route,
    Link,
    browserHistory
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'
//---------- Const ------------//
import {
    styleOpts
} from '../../constants/models';
//---------- Actions ------------//
import {
    isMain
} from '../../actions/basicActions'
//---------- Store ------------//
import store from '../../store';
//---------- Vendor ------------//



import Ink from 'react-ink';

//---------- Components ------------//


import Main from '../../containers/Main/Main.js';


//---------- Img ------------//
// import arrowPrevUrl from '../../images/global/arrow-prev.png';


//==================================================//


class Header extends Component {



    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);



    }

    handleClick() {

        // console.log('browserHistory', browserHistory);

        browserHistory.push(-1);

    }



    render() {

        const {
            header
        } = this.props.basic;



        return (

            <div className="header-comp">                     
                <div className='top-bar'>
                    <div className="prev-arrow" style={header.isMain?styleOpts.none:styleOpts.block} onClick={this.handleClick}><Ink/></div>
                    {header.text}
                </div>
            </div>
        );
    }
};

const mapStoreToProps = (state) => {

    return state;

}



export default connect(

    mapStoreToProps

)(Header)