//-------  CSS  --------//
import './mainList.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//
// import {
//     inSettle,
//     initCart
// } from '../../actions/cartActions';

// import {
//     toPartOne
// } from '../../actions/styleActions';

//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class MainListItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {



    }

    render() {



        return (
            <Link to={this.props.url}>
                <li className="main-list-li">
                    <img src={this.props.iconUrl}/>
                    <span>{this.props.text}</span>
                </li>
            </Link>
        );
    }
};


class MainList extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <MainListItem key={index} text={item.text} url={item.url} iconUrl={item.iconUrl} />
    }

    render() {


        const {
            mainListArr
        } = this.props.basic.main;

        var items = mainListArr.map(this.generateItem);
        return (
            <ul className="main-list-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(MainList);