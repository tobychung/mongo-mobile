//-------  CSS  --------//
import './navbar.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//
// import {
//     inSettle,
//     initCart
// } from '../../actions/cartActions';

// import {
//     toPartOne
// } from '../../actions/styleActions';

//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class NavBarItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {



    }

    render() {



        return (
            <li className="underline-effect">
               <Link to={this.props.url} onClick={this.handleClick}>{this.props.text}</Link>
            </li>
        );
    }
};


class NavBar extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <NavBarItem key={index} text={item.text} url={item.url} submenu={item.submenu} />
    }

    render() {

        const {
            navData
        } = this.props;


        var items = navData.map(this.generateItem);
        return (
            <ul className="menu">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(NavBar);