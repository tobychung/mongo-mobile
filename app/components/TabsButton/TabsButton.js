//-------  CSS  --------//
import './tabsButton.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//
// import {
//     inSettle,
//     initCart
// } from '../../actions/cartActions';

// import {
//     toPartOne
// } from '../../actions/styleActions';

//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class TabsButtonItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {



    }

    render() {



        return (
            <Link to={this.props.url}>
            <li className="tabs-button-li">
               <img src={this.props.imgUrl}/>
               <span>{this.props.text}</span>
            </li>
            </Link>
        );
    }
};


class TabsButton extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <TabsButtonItem key={index} url={item.url} imgUrl={item.imgUrl} text={item.text} />

    }

    render() {


        const {
            tabsArr
        } = this.props.basic.footer;

        let items = tabsArr.map(this.generateItem);

        return (
            <ul className="tabs-button-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(TabsButton);