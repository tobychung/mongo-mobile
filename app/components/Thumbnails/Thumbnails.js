//-------  CSS  --------//
import './thumbnails.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link,
    browserHistory
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//

import {
    showPart
} from '../../actions/basicActions'
//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class ThumbnailsItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this, props);

    }

    handleClick(props, event) {


        if (props.pwd == "main") {

            store.dispatch(showPart(props.index));
            browserHistory.push('/intro');

        } else {

            console.log('nonono');
        }



    }

    render() {



        return (

            <li className="thumbnails-li" onClick={this.handleClick}>
                <div className="top-title">{this.props.title}</div> 
                
                <img src={this.props.imgUrl}/>
                
                <div className='info-container'>
                    <span className="date">{this.props.date}</span>
                    <span className="title">{this.props.title}</span>
                    <h5 className="info">{this.props.info}</h5>
                </div>
               
            </li>

        );
    }
};


class Thumbnails extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <ThumbnailsItem key={index} index={index} pwd={item.pwd} url={item.url} title={item.title} date={item.date} info={item.info} imgUrl={item.imgUrl} />
    }

    render() {

        // console.log('this.props', this.props);
        var items = this.props.arr.map(this.generateItem);
        return (
            <ul className="thumbnails-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(Thumbnails);