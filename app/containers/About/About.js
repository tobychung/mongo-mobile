//-------  CSS  --------//
import './about.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//

import Thumbnails from '../../components/Thumbnails/Thumbnails.js';

//---------- Img ------------//


import bannerImg from '../../images/about/about-banner.png';
//==================================================//

class About extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }

    generateBreakLine(text, index) {

        return <p key={index}>{text}</p>
    }

    render() {

        const {
            about
        } = this.props.basic;


        let breakLines = about.info.map(this.generateBreakLine);



        return (


            <div className="about-bg bg-grp">
                
                <img className='about-banner' src={bannerImg}/>
                
                <div className="page-title">關於我們</div>

                <div className="info">{breakLines}</div>

                <div className="link">jackjack123@gmail.com</div>
                
                
                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(About)