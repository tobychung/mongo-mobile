//-------  CSS  --------//
import './game.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//

import Thumbnails from '../../components/Thumbnails/Thumbnails.js';

//---------- Img ------------//
//==================================================//

class Game extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }



    render() {

        const {
            game
        } = this.props.basic;

        return (


            <div className="game-bg bg-grp">
                
                <div className="page-title">遊戲推薦</div>
                
                <Thumbnails arr={game.thumbnailsArr} />

                
                
                
                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(Game)