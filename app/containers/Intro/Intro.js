//-------  CSS  --------//
import './intro.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//

import Thumbnails from '../../components/Thumbnails/Thumbnails.js';
import Dropdown from '../../components/Dropdown/Dropdown.js';

//---------- Img ------------//
//==================================================//


class IntroPart extends Component {



    constructor(props) {

        super(props)

    }

    render() {

        let block = {
                display: "block"
            },
            none = {
                display: "none"
            };

        // console.log('[intro] this.props', this.props);

        return (

            <div className='intro-part' style={ this.props.isVisible?block:none }>
                <Dropdown arr={this.props.dropdownArr} part={this.props.part} lord="intro"/>
            </div>


        );
    }
};



class Intro extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }

    generatePart(item, index) {

        return <IntroPart key={index} part={index} isVisible={item.isVisible} dropdownArr={item.dropdownArr}/>
    }

    render() {

        const {
            intro
        } = this.props.basic;

        let parts = intro.map(this.generatePart);


        return (


            <div className="intro-bg bg-grp">
                
                {parts}
                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(Intro)