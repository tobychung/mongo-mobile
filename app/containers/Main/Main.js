//-------  CSS  --------//
import './main.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//


import Slider from 'react-slick';

import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//

import Thumbnails from '../../components/Thumbnails/Thumbnails.js';
import MainList from '../../components/MainList/MainList.js';

//---------- Img ------------//


import carousel01 from '../../images/main/main-carousel-01.png';
//==================================================//

class Main extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }



    render() {

        const {
            main
        } = this.props.basic;

        let settings = {
            dots: false,
            fade: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,

        };


        return (


            <div className="main-bg bg-grp">
                
                <Slider {...settings}>
                    <div><img src={carousel01} /></div>
                </Slider>

                <div className='btn-container'>
                    <Link to="/video" ><div className="btn-01 btn-grp">視頻教學</div></Link>
                    <Link to="/game" ><div className="btn-02 btn-grp">遊戲推薦</div></Link>
                </div>
                
                <Thumbnails arr={main.thumbnailsArr} />

                <MainList/>    
                
                
                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(Main)