//-------  CSS  --------//
import './video.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';


//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//

import Thumbnails from '../../components/Thumbnails/Thumbnails.js';
import Dropdown from '../../components/Dropdown/Dropdown.js';


//---------- Img ------------//
//==================================================//

class Video extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }



    render() {

        const {
            video
        } = this.props.basic;

        return (


            <div className="video-bg bg-grp">
                
                <div className="page-title">視訊教學</div>

                <Dropdown arr={video.dropdownArr}  lord="video"/>
                
                
                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(Video)