//-------  CSS  --------//
import './vendor/css/bootstrap.min.css';
import './vendor/css/font-awesome.css';

import './vendor/css/ajax-loader.gif';
import './vendor/css/slick-theme.min.css';
import './vendor/css/slick.min.css';


import './base/base.min.css';
import "./base/global.min.css";

//-------  Basic  --------//
import React, {
    Component,
    PropTypes
} from 'react'

import ReactDOM from 'react-dom';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';
//-------  Redux  --------//
import {
    createStore
} from 'redux';

import {
    Provider
} from 'react-redux';


import axios from 'axios';

//---------- Store ------------//
import store from './store';

//---------- Vendor ------------//

import BurgerMenu from 'react-burger-menu';
import classNames from 'classnames';
import DocumentMeta from 'react-document-meta';

//---------- Components ------------//

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

//---------- Img ------------//

import faviconUrl from './images/global/favicon.ico';


//---------- RWD ------------//

import "./base/media-query.min.css";

//==================================================//

let MenuWrap = React.createClass({

    getInitialState() {
        return {
            hidden: false
        };
    },

    componentWillReceiveProps(nextProps) {
        const sideChanged = this.props.children.props.right !== nextProps.children.props.right;

        if (sideChanged) {
            this.setState({
                hidden: true
            });

            setTimeout(() => {
                this.show();
            }, this.props.wait);
        }
    },

    show() {
        this.setState({
            hidden: false
        });
    },

    render() {
        let style;

        if (this.state.hidden) {
            style = {
                display: 'none'
            };
        }

        return (
            <div style={style} className={this.props.side}>
        {this.props.children}
      </div>
        );
    }
});


//==================================================//



class Index extends Component {


    constructor(props) {
        super(props);
        this.state = {
            currentMenu: 'push',
            side: 'left',
            isOpen: false
        }
    }

    changeMenu(menu) {
        this.setState({
            currentMenu: menu
        });
    }

    changeSide(side) {
        this.setState({
            side
        });
    }

    closeMenu() {
        this.setState({
            isOpen: false
        });
    }


    getItems() {
        let items;

        const menus = {
            slide: {
                buttonText: 'Slide',
                items: 1
            },
            stack: {
                buttonText: 'Stack',
                items: 1
            },
            elastic: {
                buttonText: 'Elastic',
                items: 1
            },
            bubble: {
                buttonText: 'Bubble',
                items: 1
            },
            push: {
                buttonText: 'Push',
                items: 1
            },
            pushRotate: {
                buttonText: 'Push Rotate',
                items: 2
            },
            scaleDown: {
                buttonText: 'Scale Down',
                items: 2
            },
            scaleRotate: {
                buttonText: 'Scale Rotate',
                items: 2
            },
            fallDown: {
                buttonText: 'Fall Down',
                items: 2
            }
        };



        // switch (menus[this.state.currentMenu].items) {
        //     case 1:
        //         items = [
        //             <a key="0" href=""><i className="fa fa-fw fa-star-o" /><span>Favorites</span></a>,
        //             <a key="1" href=""><i className="fa fa-fw fa-bell-o" /><span>Alerts</span></a>,
        //             <a key="2" href=""><i className="fa fa-fw fa-envelope-o" /><span>Messages</span></a>,
        //             <a key="3" href=""><i className="fa fa-fw fa-comment-o" /><span>Comments</span></a>,
        //             <a key="4" href=""><i className="fa fa-fw fa-bar-chart-o" /><span>Analytics</span></a>,
        //             <a key="5" href=""><i className="fa fa-fw fa-newspaper-o" /><span>Reading List</span></a>
        //         ];
        //         break;
        //     case 2:
        //         items = [
        //             <h2 key="0"><i className="fa fa-fw fa-inbox fa-2x" /><span>Sidebar</span></h2>,
        //             <a key="1" href=""><i className="fa fa-fw fa-database" /><span>Data Management</span></a>,
        //             <a key="2" href=""><i className="fa fa-fw fa-map-marker" /><span>Location</span></a>,
        //             <a key="3" href=""><i className="fa fa-fw fa-mortar-board" /><span>Study</span></a>,
        //             <a key="4" href=""><i className="fa fa-fw fa-picture-o" /><span>Collections</span></a>,
        //             <a key="5" href=""><i className="fa fa-fw fa-money" /><span>Credits</span></a>
        //         ];
        //         break;
        //     default:
        //         items = [
        //             <a key="0" href=""><i className="fa fa-fw fa-star-o" /><span>Favorites</span></a>,
        //             <a key="1" href=""><i className="fa fa-fw fa-bell-o" /><span>Alerts</span></a>,
        //             <a key="2" href=""><i className="fa fa-fw fa-envelope-o" /><span>Messages</span></a>,
        //             <a key="3" href=""><i className="fa fa-fw fa-comment-o" /><span>Comments</span></a>,
        //             <a key="4" href=""><i className="fa fa-fw fa-bar-chart-o" /><span>Analytics</span></a>,
        //             <a key="5" href=""><i className="fa fa-fw fa-newspaper-o" /><span>Reading List</span></a>
        //         ];
        // }


        items = [
            <Link to="/" key="0" onClick={this.closeMenu}><i className="fa fa-fw fa-home" /><span>首頁</span></Link>,
            <Link to="/news" key="1" onClick={this.closeMenu}><i className="fa fa-fw fa-rss" /><span>最新消息</span></Link>,
            <Link to="/brew" key="2" onClick={this.closeMenu}><i className="fa fa-fw fa-envelope-o" /><span>介紹</span></Link>,
            <Link to="/class" key="3" onClick={this.closeMenu}><i className="fa fa-fw fa-comment-o" /><span>課程</span></Link>,
            <Link to="/product" key="4" onClick={this.closeMenu}><i className="fa fa-fw fa-rocket" /><span>線上商品</span></Link>,
            <Link to="/cart" key="5" onClick={this.closeMenu}><i className="fa fa-fw fa-shopping-cart" /><span>購物車</span></Link>
        ];



        return items;
    }



    getMenu() {
        const Menu = BurgerMenu[this.state.currentMenu];
        const items = this.getItems();
        let jsx;

        if (this.state.side === 'right') {
            jsx = (
                <MenuWrap wait={20} side={this.state.side}>
              <Menu id={this.state.currentMenu} width={ 200 } pageWrapId={'page-wrap'} outerContainerId={'outer-container'} right>
                {items}
              </Menu>
            </MenuWrap>
            );
        } else {
            jsx = (
                <MenuWrap wait={20}>
              <Menu id={this.state.currentMenu} width={ 200 } pageWrapId={'page-wrap'} outerContainerId={'outer-container'}>
                {items}
              </Menu>
            </MenuWrap>
            );
        }

        return jsx;
    }



    render() {

        let meta = store.getState().basic.metaObj;

        return (
            <Provider store={store}>
                <div id="outer-container" style={{height: '100%'}}>
                    {/*this.getMenu()*/}
                    <div id="page-wrap" className="global-bg">
                        <DocumentMeta {...meta} />
                       
                        <Header/>

                        {
                            this.props.children
                        }

                        <Footer/>
                    </div>
                </div>
            </Provider>
        );
    }
};

export default Index;