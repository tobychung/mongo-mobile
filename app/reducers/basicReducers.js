//-------  Basic  --------//
import Immutable from 'immutable';

import {
    handleActions
} from 'redux-actions';

//-------  Initial State  --------//
import {
    initialBasicState
} from '../constants/models';

//==================================================//


export default handleActions({

    SET_ALL_DATA: (state, {
        payload
    }) => {



        const newState = Immutable.fromJS(state)
            // .set('brewItemsArr', resultArr)
            .toJS();

        return newState;

    },

    SHOW_PART: (state, {
        payload
    }) => {

        console.log('state', state);
        console.log('payload', payload);

        const newIntroArr = [...state.intro];



        for (let item of newIntroArr) {

            item.isVisible = false;

        }

        newIntroArr[payload].isVisible = true;



        const newState = Immutable.fromJS(state)
            .set('intro', newIntroArr)
            .toJS();

        return newState;

    },



    IS_MAIN: (state, {
        payload
    }) => {


        const newState = Immutable.fromJS(state)
            .setIn(['header', 'isMain'], payload)
            .toJS();



        return newState;

    },

    TOGGLE_INTRO_LIST: (state, {
        payload
    }) => {

        // console.log('state', state, payload);

        let part = payload.part,
            index = payload.index,
            newIntroList = [...state.intro];


        newIntroList[part].dropdownArr[index].toggle = !newIntroList[part].dropdownArr[index].toggle;



        // console.log('newIntroList', newIntroList);

        const newState = Immutable.fromJS(state)
            .set('intro', newIntroList)
            .toJS();


        // console.log('newState', newState);

        return newState;

    },

    TOGGLE_VIDEO_LIST: (state, {
        payload
    }) => {

        let index = payload.index,
            newVideoList = [...state.video.dropdownArr];

        newVideoList[index].toggle = !newVideoList[index].toggle;


        const newState = Immutable.fromJS(state)
            .setIn(['video', 'dropdownArr'], newVideoList)
            .toJS();

        return newState;

    },


}, initialBasicState);