//-------  Basic  --------//
import React, {
    Component,
    PropTypes
} from 'react';
import ReactDOM from 'react-dom';
//-------  Router  --------//
import {
    Router,
    Route,
    Link,
    browserHistory,
    IndexRoute
} from 'react-router';

import {
    createHashHistory
} from 'history';

//---------- Actions ------------//
import {
    isMain
} from './actions/basicActions'
//---------- Store ------------//
import store from './store';

//---------- Vendor ------------//

import DocumentMeta from 'react-document-meta';

//---------- Components ------------//

import Index from './index.js';
import Main from './containers/Main/Main.js';
import Video from './containers/Video/Video.js';
import Game from './containers/Game/Game.js';
import Event from './containers/Event/Event.js';
import About from './containers/About/About.js';
import Intro from './containers/Intro/Intro.js';

//==================================================//

ReactDOM.render((


    <Router history = {browserHistory}>
        <Route path = "/" component = {Index} onChange={(prevState, nextState, callback) => {
            
            let destPath = nextState.location.pathname;

            if(destPath =="/"){

                store.dispatch(isMain(true)); 
            }else{

                store.dispatch(isMain(false)); 
            }


            // console.log('page',nextState.location.pathname);

        }}>
            <IndexRoute component = {Main}/>
            <Route path = "video" component = {Video} />
            <Route path = "game" component = {Game} />
            <Route path = "event" component = {Event} />
            <Route path = "about" component = {About} />
            <Route path = "intro" component = {Intro} />

        </Route>
    </Router>


), document.getElementById('body'));