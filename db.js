module.exports = function(app) {
    var env = app.get('env');
    var mongoose = require('mongoose');

    if ('development_local' == env) {
        mongoose.connect('mongodb://localhost/testDB');
    } 

    // else if ('development_heroku' == env) {
    //     mongoose.connect(process.env.MONGOLAB_URI);
    // } else if ('deployed_beta_aws' == app.get('env')) {
    //     mongoose.connect('mongodb://localhost/MongoDB');
    // }


    mongoose.connection.on('error', function(err) {
        console.log(err);
    });

    mongoose.connection.once('open', function() {
        console.log('DB connection established');
    });

    require('./models/Articles');
    // require('./models/user');
    // require('./models/unit_brands');
    // require('./models/personalBeverage');
    // require('./models/transactions');
    // require('./models/items_registList');
    // require('./models/tags');
    // require('./models/image');
    // require('./models/comments');
};